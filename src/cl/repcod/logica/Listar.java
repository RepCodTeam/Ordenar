/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.repcod.logica;

import cl.repcod.bean.Elemento;
import cl.repcod.bean.Ordenados;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Uthanien
 */
public class Listar {
    
    /**
     * Metodo que asigna "algo" a un "distino"
     * @param destino
     * @param porAsignar
     * @return 
     */
    public static List<Ordenados> asignarAmigo(List<Ordenados> destino, List<Elemento> porAsignar){
        int tamañoDestino = destino.size();
        int tamañoAsignar = porAsignar.size();
        List<Ordenados> listaOrdenada = destino;
        NumeroAleatorios numeros = new NumeroAleatorios(0, tamañoDestino-1);
        
        for(int in = 0; in < tamañoAsignar; in++){
            listaOrdenada.get(numeros.generar()).setAsignado(porAsignar.get(in).getNombre());
        }   
        
        return listaOrdenada;
    }
    
    
}
