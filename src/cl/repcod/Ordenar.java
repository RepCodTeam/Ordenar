/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.repcod;

import cl.repcod.bean.Elemento;
import cl.repcod.bean.Ordenados;
import cl.repcod.logica.Listar;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Uthanien
 */
public class Ordenar {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        List<Ordenados> destino = new ArrayList();
        List<Elemento> porAsignar = new ArrayList();
        destino.add(new Ordenados(1, "Grupo 2"));
        destino.add(new Ordenados(2, "Grupo 4"));
        porAsignar.add(new Elemento(1, "Mario Beoriza"));
        porAsignar.add(new Elemento(2, "Miguel Plaza"));

        
        //Nuevo Ordenar
        List<Ordenados> resultado = Listar.asignarAmigo(destino, porAsignar);
        for(Ordenados e : resultado){
            System.out.println(e.getNombre() + " - " + e.getAsignado());
        }
    }    
}
