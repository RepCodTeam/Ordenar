/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.repcod.bean;

/**
 *
 * @author fespinosa
 */
public class Ordenados {

    private int id;
    private String nombre;
    private String asignado;
    
    public Ordenados(int id, String nombre, String asignado) {
        this.id = id;
        this.nombre = nombre;
        this.asignado = asignado;
    }
    
    public Ordenados(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
        this.asignado = "";
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAsignado() {
        return asignado;
    }

    public void setAsignado(String asignado) {
        this.asignado = asignado;
    }
    
}
